package mock.car;

import mock.car.parts.Motor;
import mock.car.parts.Tank;

public class Car {
    private Motor motor;
    private Tank tank;

    public Car(Motor motor) {
        this.motor = motor;
        this.tank = null;
    }
    
    public Car(Tank tank) {
        this.motor = null;
        this.tank = tank;
    }
    
    public Car(Motor motor, Tank tank) {
        this.motor = motor;
        this.tank = tank;
    }
    
    public boolean isClean() {
    	return true;
    }
    
    public boolean isTankFull() {
    	return this.tank.isFull();
    }
    
    public void wash(CarWash carwash) {
    	carwash.wash(this);
    }

    public void start() {
        motor.powerOn();
    }
    
    public void addFuel(float litersOfFuel) {
    	this.tank.addFuel(litersOfFuel);
    }
}
