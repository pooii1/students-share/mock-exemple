package mock.car.parts;

public interface Motor {
	
	void powerOn();
}
