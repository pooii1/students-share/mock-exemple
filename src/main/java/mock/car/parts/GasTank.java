package mock.car.parts;

public class GasTank implements Tank {

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public void addFuel(float numberOfLiters) {
		// Add fuel
	}
}
