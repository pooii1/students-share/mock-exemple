package mock.car.parts;

public class CombustionMotor implements Motor {
    private Battery battery;
    private SparkPlug plug;

    public CombustionMotor(Battery battery, SparkPlug plug) {
        if (battery.isRecentEnough()) {
        this.battery = battery;
        } else {
            throw new FailingBatteryException("Battery is too old");
        }
        this.plug = plug;
    }

    public void powerOn() {
        battery.powerOn();
        plug.lightOn();
    }
}
