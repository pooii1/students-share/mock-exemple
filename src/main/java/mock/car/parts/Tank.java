package mock.car.parts;

public interface Tank {
	
	boolean isFull();
	
	void addFuel(float numberOfLiters);
}
