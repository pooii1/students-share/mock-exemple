package mock.car.parts;

import java.time.LocalDate;

public class ElectricBattery implements Battery {

	private static final int MAXIMUM_YEARS_BATTERY_LIFE = 5;
	private int yearOfCreation;
	private boolean isStarted = false;

	public ElectricBattery(int yearOfCreation) {
		this.yearOfCreation = yearOfCreation;
		this.isStarted = false;
	}
	
	public boolean isStarted() {
		return isStarted;
	}

	public boolean isRecentEnough() {
		return (LocalDate.now().getYear() - this.yearOfCreation) <= MAXIMUM_YEARS_BATTERY_LIFE;
	}

	public void powerOn() {
		isStarted = true;
	}

}
