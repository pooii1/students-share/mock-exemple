package mock.car.parts;

public interface Battery {
	
	boolean isStarted();
	
	boolean isRecentEnough();
	
	void powerOn();
}
