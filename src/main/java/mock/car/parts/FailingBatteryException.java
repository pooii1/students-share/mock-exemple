package mock.car.parts;

public class FailingBatteryException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public FailingBatteryException(String message) {
		super(message);
	}	
}
