package mock.car.dummy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import mock.car.Car;
import mock.car.CarWash;

class CarTestWithDummyMock {	

	/*
	 * Quand utiliser un Mock de type 'Dummy'
	 */
	@Test
	void whenWashingCarThenCarIsClean() {
		DummyMotor motor = new DummyMotor();
	    Car car = new Car(motor);  // Nous avons besoin d'un motor pour créer notre objet à tester, mais le moteur n'est pas impliqué dans le lavage de la voiture.
	    CarWash carwash = new CarWash();

	    car.wash(carwash);

	    Assertions.assertTrue(car.isClean());		
	}
}
