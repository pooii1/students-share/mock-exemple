package mock.car.spy;

import mock.car.parts.Tank;

public class SpyTank implements Tank {
    public float numberOfLitersAdded = 0;
    public int numberOfCallsToAddFuel = 0;

    public SpyTank() { }

    // Les valeurs-témoins du mock sont mises à jour avec l'appel de la méthode.
    public void addFuel(float litersOfFuel) {
        this.numberOfLitersAdded = litersOfFuel;
        this.numberOfCallsToAddFuel++;
    }

	@Override
	public boolean isFull() {
		return false;
	}
}
