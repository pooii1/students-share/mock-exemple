package mock.car.spy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import mock.car.Car;

class CarTestWithSpy {	

	@Test
	void whenTankIsFullThenCarIsFull() {  
	    SpyTank spyTank = new SpyTank();
	    Car car = new Car(spyTank);
	    
	    car.addFuel(3.5f);

	    // On vérifie l'information accumulée dans le SpyMock
	    Assertions.assertEquals(3.5f, spyTank.numberOfLitersAdded);
	    Assertions.assertEquals(1, spyTank.numberOfCallsToAddFuel);
	}
}
