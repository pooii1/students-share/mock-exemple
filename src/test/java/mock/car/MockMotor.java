package mock.car;

import mock.car.parts.Motor;

public class MockMotor implements Motor {

	public boolean isPoweredOn = false;
	
	@Override
	public void powerOn() {
		isPoweredOn = true;
	}
}
