package mock.car;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import mock.car.parts.Battery;
import mock.car.parts.CombustionMotor;
import mock.car.parts.ElectricBattery;
import mock.car.parts.GoodOldSparkPlug;
import mock.car.parts.Motor;
import mock.car.parts.SparkPlug;

class CarTest {	

	/*
	 * Test avec toutes les dépendances, sans mock, pour tester si la voiture est démarrée
	 */
	@Test
	void whenStartingCarThenCarMotorIsPowerOn() {
		// Beaucoup de dépendances à créer pour tester notre voiture (une batterie, une bougie, un moteur, etc.)	    
	    Battery battery = new ElectricBattery(2018);  
	    SparkPlug sparkPlug = new GoodOldSparkPlug();
	    Motor motor = new CombustionMotor(battery, sparkPlug);
	    Car car = new Car(motor);

	    car.start();

	    //On vérifie si notre batterie est partie! Notez que nous sommes dans la classe Car et que nous devons nous fier sur la batterie pour faire notre vérification
	    // -> C'est une dépendance!
	    Assertions.assertTrue(battery.isStarted());		
	}
	
	/*
	 * Même test, mais avec un Mock pour 'couper' la dépendance et les effets indésirables!
	 */
	@Test
	void whenStartingCarWithMockThenCarMotorIsPowerOn() {
		MockMotor motor = new MockMotor(); //Juste avec cette ligne, on vient d'enlever les dépendances sur la batterie, la bougie et le 'vrai' moteur!.
	    Car car = new Car(motor);

	    car.start();

	    //On fait juste vérifier si la méthode powerOn() de Motor est appelé.  Le reste sera testé dans les tests de Motor!
	    Assertions.assertTrue(motor.isPoweredOn);		
	}
}
