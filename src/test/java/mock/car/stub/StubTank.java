package mock.car.stub;

import mock.car.parts.Tank;

public class StubTank implements Tank {

	@Override
	public boolean isFull() {
		// La valeur de retour est hard-coded à vrai
		return true;
	}

	@Override
	public void addFuel(float numberOfLiters) {		
	}
}
