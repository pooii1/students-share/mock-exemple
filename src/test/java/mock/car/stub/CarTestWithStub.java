package mock.car.stub;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import mock.car.Car;

class CarTestWithStub {	

	@Test
	void whenStartingCarWithMockThenCarMotorIsPowerOn() {
		StubTank tank = new StubTank();
	    Car car = new Car(tank);

	    //isFull sera toujours vrai, parce que StubTank retourne toujours vrai pour isFull();
	    boolean isFull = car.isTankFull();

	    Assertions.assertTrue(isFull);		
	}
}
