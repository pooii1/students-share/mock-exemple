package mock.car.fake;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import mock.car.Car;

class CarTestWithFake {	

	@Test
	void whenFillingCarFullThenCarIsFull() {
		FakeTank tank = new FakeTank(true);
	    Car car = new Car(tank);

	    //isFull est vrai, parce que FakeTank a été construit avec 'true';
	    boolean isFull = car.isTankFull();

	    Assertions.assertTrue(isFull);		
	}
	
	@Test
	void whenFillingCarHalfwayThenCarIsNotFull() {
		FakeTank tank = new FakeTank(false);
	    Car car = new Car(tank);

	    //isFull est faux, parce que FakeTank a été construit avec 'false';
	    boolean isFull = car.isTankFull();

	    Assertions.assertFalse(isFull);		
	}
}
