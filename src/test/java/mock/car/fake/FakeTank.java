package mock.car.fake;

import mock.car.parts.Tank;

//Ce mock permet de vérifier 2 cas: quand le réservoir est plein et quand il n'est pas plein.
//Ça donne une petite flexibilité.
public class FakeTank implements Tank {
	
	private boolean isFull = false;
	
	public FakeTank(boolean isSupposedToBeFull) {
		this.isFull = isSupposedToBeFull;
	}

	@Override
	public boolean isFull() {
		return isFull;
	}

	@Override
	public void addFuel(float numberOfLiters) {
		
	}
}
