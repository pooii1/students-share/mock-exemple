# Exemple de Mock

Projet d'exemple pour illustrer l'utilisation des Mocks.  Code complet des exemples utilisés dans les notes de cours.

## Installation

Clonez (ou téléchargez) le projet et importez-le dans votre IDE préféré comme projet Maven.

## Licence

GNU
